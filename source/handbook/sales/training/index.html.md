---
layout: handbook-page-toc
title: "Sales Training"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# **GitLab Sales Learning Framework**
GitLab sales team members are expected to be knowledgeable and proficient across a variety of topics, skills, behaviors, and processes. For ease of consumption, sales training resources are organized in the following 6 categories below. If you are looking for more immediate access to resources to support your selling efforts, please check out the [Sales Resources](/handbook/marketing/product-marketing/sales-resources/) page.

| **Category** | **Description** |
| ---------------------------------------- | ----------------------------------------- |
| **[GitLab Target Audiences](/handbook/sales/training/#gitlab-target-audiences)** | Successful sales team members must have an intimate understand of ideal customer profiles, customer environments where opportunities for GitLab are most ripe, targeted buyer personas, industry-relevant insights, and more |
| **[Industry Topics & Trends](/handbook/sales/training/#industry-topics--trends)** | To serve as a trusted advisor, GitLab sales team members must be able to competently and confidently engage with customers on topics like SDLC, SCM/VCS, open source & open core, DevOps, CI/CD, containers, Kubernetes, and more |
| **[Why GitLab](/handbook/sales/training/#why-gitlab)** | GitLab sales team members must be able to clearly articulate a compelling, differentiated, value-driven message to customers, prospects, and partners |
| **[Competition](/handbook/sales/training/#competition)** | To maximize opportunities, GitLab sales team members must demonstrate the knowledge and ability to beat out various competitors |
| **[GitLab Portfolio](/handbook/sales/training/#gitlab-portfolio)** | GitLab sales team members must understand how various elements of the GitLab portfolio (offering tiers, SDLC phases, services, training, etc.) solve customer challenges |
| **[Functional Skills & Processes](/handbook/sales/training/#functional-skills--processes)** | To optimize productivity, GitLab sales team members must be able to consistently demonstrate the sales skills and behaviors (including adherence to standard processes and use of tools like Salesforce and others) that lead to desired outcomes |

## **GitLab Target Audiences**
*  [Understanding the GitLab Buyer: Personas and Pain Points](https://youtu.be/-UITZi0mXeU) (Jan 2018, 10 minutes)
*  [GitLab Buyer Personas](/handbook/marketing/product-marketing/roles-personas/#buyer-personas)
*  Learn more about [Enterprise IT roles & key care-abouts](/handbook/marketing/product-marketing/enterprise-it-roles/)

## **Industry Topics & Trends**
*  [Understand the Industry where GitLab Competes](https://www.youtube.com/watch?v=qQ0CL3J08lI) (Jan 2018, 11.5 minutes)
*  [What is DevOps?](https://www.youtube.com/watch?v=_I94-tJlovg) (Dec 2013, 7 minutes)
*  [DevOps Explained](/devops/) (20 minutes)
*  [How is software made using GitLab?](https://www.youtube.com/watch?v=UuX-GnYWNwo&feature=youtu.be) (Nov 2019, 20 minutes)
*  [Software Development Lifecycle (SDLC)in 9 minutes!](https://www.youtube.com/watch?v=i-QyW8D3ei0) (Jan 2016, 9 minutes)
*  [What is the Software Development Lifecycle (SDLC)?](https://www.youtube.com/watch?v=Ancdhr3t2sE) (July 2018, 11 minutes)
*  [GitLab's public website on the SDLC](/sdlc/)
*  Review the [DevOps Tool Landscape](/comparison/)
*  [Git Tutorials](https://www.youtube.com/playlist?list=PLu-nSsOS6FRIg52MWrd7C_qSnQp3ZoHwW)
*  Review and subscribe to the following blogs: [Hacker News](https://news.ycombinator.com/), [Martin Fowler](https://martinfowler.com/), and [New Stack](https://thenewstack.io/) (15 minutes)
*  [Cloud-Native Transformation](https://www.youtube.com/watch?v=WsIM034RnAc) (Mar 2019, 29 minutes)
*  [What are Microservices?](https://www.youtube.com/watch?v=petnTitp6CQ) (Jan 2019, 19 minutes)
*  [What is Serverless?](https://www.youtube.com/watch?v=GBOroGozm5w) (Jan 2019, 18 minutes)
*  [Key Findings of Annual DORA (DevOps Research & Assessment) Survey](https://www.youtube.com/watch?v=VBsQmE8LIYM) (Dec 2018, 12 minutes)
*  [Cloud-Native Ecosystem](https://www.youtube.com/watch?v=0DxQKVKB3nY) (Oct 2018, 19 minutes)
*  [Introduction to GitLab with GitBasics](https://www.youtube.com/watch?v=RDOqeTp_u4A) (Jun 2018, 86.5 minutes)

## **Why GitLab?**
*  [Delivering the GitLab pitch deck](https://drive.google.com/open?id=1vRgU1o-o4kcOblQCxNi3h6xrN7KQZY1H) (Apr 2019, 14 minutes)
*  [GitLab’s key value drivers and differentiators](/handbook/sales/command-of-the-message/)
*  [Keys to Faster Delivery (Accelerate Digital Transformation)](https://youtu.be/MwSJuKYXAy4) (Jul 2019, 18.5 minutes)
*  [How GitLab.com Subscriptions Work](https://www.youtube.com/watch?v=W-ZYi4H4XMM) (Jun 2019, 29 minutes)
*  [GitLab 101](https://www.youtube.com/watch?v=6IvHb0sV9Bc) reseller enablement webcast (Jun 2018, 59 minutes)
*  [Removing Software Bottlenecks](https://www.youtube.com/watch?v=RT-fKTFevEY) (Mar 2019, 30 minutes)
*  Review [GitLab’s Direction](/direction/#single-application)
*  Review and subscribe to the [GitLab Blog](/blog/)

## **Competition**
*  [GitLab Comparison Page](/devops-tools/)
*  [Crayon Market and Competitive Intelligence Tool Launch](https://youtu.be/qCKj6zB-Ebk) (Sep 2019, 30 minutes)
   - [Competitive Battlecards on Crayon](https://app.crayon.co/intel/gitlab/battlecards/) (log in via Okta SSO!)
*  Azure DevOps
   - [Azure DevOps Competitive Review: Part 1 of 2](https://www.youtube.com/watch?v=T-zfXQpvtAw) (Nov 2018, 23 minutes)
   - [Azure DevOps Competitive Review: Part 2 of 2](https://www.youtube.com/watch?v=NeMzSOg7dV4) (Nov 2018, 13 minutes)
*  Jenkins   
   - [Competing Against Jenkins and CloudBees](https://youtu.be/a95DQqRTOHw) (Sep 2019, 25 minutes)
   - [Jenkins: Competitive Overview](https://www.youtube.com/watch?v=_HJMtp-fcHc) (Feb 2019, 31 minutes)
   - [Jenkins: Overcoming Objections](https://www.youtube.com/watch?v=qrENxT9iPvE) (Mar 2019, 26 minutes)
   - [GitLab vs Jenkins, where is GitLab _weaker_?](https://www.youtube.com/watch?v=3Wr3O6mY5VE) (Feb 2019, 25.5 minutes)

## **GitLab Portfolio**
*  [GitLab Product Vision](https://www.youtube.com/watch?v=RCMr7i3zSwM) (Apr 2019, 30 minutes)
*  [Use Case Session 1: Increase the quality of my code while decreasing time to delivery (Continuous Integration (CI))](https://youtu.be/us1hjgfX8hE) (Sep 2019, 26.5 minutes)
*  [How to upgrade your customer to a higher tier](https://www.youtube.com/watch?v=8ZpU7PZzFyY) (Jul 2019, 26 minutes)
*  [GitLab Ultimate is Ready for Primetime](https://www.youtube.com/watch?v=3M8SIeykbrM) (Nov 2019, 31 minutes)
*  [Agile Project & Portfolio Management on GitLab Click-Through Demo](https://www.youtube.com/watch?v=Eo8pFoE6DjU) (Jun 2019, 30 minutes)
*  [GitLab AutoDevOps Run Click-Through Demo](https://www.youtube.com/watch?v=V_6bR0Kjju8) (Apr 2019, 21 minutes)
*  [Q3FY20 GitLab Release Update for Sales](https://www.youtube.com/watch?v=y2KrovJD76Q) (Aug 2019, 29 minutes)
*  [Forrester Wave Cloud CI Report: What It Means to GitLab Sales, Customers & Prospects](https://youtu.be/q6YXIvHZuDU) (Oct 2019, 32 minutes)
*  [Selling Services to Accelerate Customer Adoption](https://youtu.be/ngTI5-1cQK4) (Oct 2019, 28 minutes)
*  Security
   - [GitLab Security & Compliance Capabilities](https://www.youtube.com/watch?v=-e4V9j0g80A) (Jan 2019, 37 minutes)
   - [GitLab Security Features: Part 1](https://www.youtube.com/watch?v=-1-giCSuXF4) (Dec 2018, 27 minutes)
   - [GitLab Security Features: Part 2](https://www.youtube.com/watch?v=VVzSToclmuk) (Dec 2018, 31 minutes)
   - [Forrester Software Composition Analysis (SCA) Wave & Security Q&A](https://www.youtube.com/watch?v=FIfZCg02G3o) (Apr 2019, 30 minutes)
   - [GitLab Security Deep Dive](https://www.youtube.com/watch?v=k4vEJnGYy84) (Oct 2018, 27 minutes)
   - [GitLab’s Security Dashboard](https://www.youtube.com/watch?v=95gndJnvukA) (Nov 2018, 32 minutes)
*  [GitLab: Open Source vs. Commercial Offering](https://www.youtube.com/watch?v=vb3Fgs6q_rg) (Dec 2018, 7.5 minutes)
*  [Meltano Demo](https://www.youtube.com/watch?v=-wpAb7tnQRo) (Nov 2018, 18.5 minutes)
*  [CI/CD Tools Primer](https://www.youtube.com/watch?v=8s9uZhYr0T4) (Aug 2018, 26.5 minutes)
*  [Introduction to GitLab CI/CD](https://www.youtube.com/watch?v=Dk3UT7oLsBA) (Jun 2018, 53 minutes)
*  [GitLab Product Tiers & Maturity Levels](https://www.youtube.com/watch?v=9-o5eheXpho) (Jun 2018, 31 minutes)

## **Functional Skills & Processes**
*  [Critical GitLab Sales Skills and Behaviors](https://youtu.be/K3h33xFXpow) video (Jul 2019, 9.5 minutes)
*  [Anatomy of a Successful Discovery Call](https://youtu.be/maai4tuDtoM) video (Jul 2019, 21 minutes)
*  [Things I Wish I Knew During My First Few Quarters at GitLab](https://youtu.be/3gprWrDTEQM) video (Aug 2019, 25.5 minutes)
*  Study and bookmark the [Sales Handbook page](/handbook/sales/)
*  [Sales Forecasting at GitLab](/handbook/sales/#weekly-forecasting)
*  [Version.GitLab.Com Walk-Through](https://www.youtube.com/watch?v=lBWwlbd1J5k) (Jul 2019, 8.5 minutes)
*  Chorus
   - [How to find a call in Chorus](https://hello.chorus.ai/listen?guid=10c8460049f842e99477de4e9f2affca) (Jul 2019, 4 minutes)
   - [How to review a call in Chorus](https://hello.chorus.ai/listen?guid=5fc22bca9ec04141a5245062b7907f09) (Jul 2019, 7 minutes)
   - [How to comment and share moments in Chorus](https://hello.chorus.ai/listen?guid=199654f39cbd4fc69c00e839ea406930) (Jul 2019, 5 minutes)
   - [How to gain compliance on recorded calls (primarily for NA)](https://hello.chorus.ai/listen?guid=6d8a5c1bcb364547bb8dc2eed8a5e6a5) (Jul 2019, 6 minutes)
*  [Clari Sales Forecasting & Pipeline Management Tool](https://app.clari.com/)
   - Join the [Google Classroom](https://classroom.google.com) for in-depth training 
   - On the top right of the Classes page, click + then  > Join class
   - Enter the Class Code: **e7hfgg0** 
*  Social Selling Basics
   - [Social Selling Basics presentation](https://docs.google.com/presentation/d/1UCRF6PC6al8XxT8E_4rDKkQjkW6WGPA6gybWeuRIg7A/edit?usp=sharing)
   - [Social Selling Basics video](https://youtu.be/w-C4jts-zUw) (Jul 2019, 20 minutes)
      - [Social Selling_Sales Enablement_2019-07-11](https://www.youtube.com/watch?v=Ir7od3stk70) (Jul 2019, 28 minutes)
   - [LinkedIn Sales Navigator resources](https://docs.google.com/document/d/1UF69ieck4AdHadzgPmZ5X1GBs3085JhlYaMowLj0AOg/edit?usp=sharing)
*  [How to Host a GitLab Meetup](https://www.youtube.com/watch?v=h24aS5rpfmM) (May 2019, 36.5 minutes)
*  [Just Commit marketing campaign overview](https://www.youtube.com/watch?v=58qDalA5o6Q) (Feb 2019, 11.5 minutes)
*  [GitLab Code Contributor Program overview](https://www.youtube.com/watch?v=6TLvvrm28NE) (Jan 2019, 13 minutes)
*  [Giving Feedback to GitLab Product Management](https://www.youtube.com/watch?v=MriV2P57LU8) (Oct 2018, 15 minutes)
*  [GitLab Customer Reference Program](https://www.youtube.com/watch?v=i7Lrup57Jzw) (Aug 2018, 30 minutes)
*  [Customer Case Studies: How Three Customers Leverage GitLab to Gain a Competitive Advantage](https://www.youtube.com/watch?v=f-DkBazKA3A) (Oct 2018, 37 minutes)
*  [How to Use Customer Case Stories](https://www.youtube.com/watch?v=yZFpUiRznqE) (Oct 2018, 5 minutes)
*  [Analyst Relations 101](https://www.youtube.com/watch?v=ZyyBq3_rzJo) (Aug 2018, 23.5 minutes)
*  [Understanding Analyst Reports and How to Use Them](https://www.youtube.com/watch?v=VzUH-IMSZ-A) (Oct 2018, 14 minutes)

# **Sales Enablement Sessions**
Live sales enablement videocasts are held every Thursday from 12:00-12:30pm ET with the exception of the last two weeks of every quarter. Sessions are recorded and published to YouTube, made available for on-demand playback, and added to the inventory above. [Learn more here](/handbook/sales/training/sales-enablement-sessions/).

# **SDR Coaching**
Similar sessions are held for GitLab’s SDR team every Wednesday from 12:30-1:00pm ET. Learn more [here](/handbook/sales/training/sdr-coaching). 

# **Additional Notes**
*  Learn about GitLab customer training on the [GitLab Training Tracks site](/training/)
*  Learn about the [weekly GitLab Sales Enablement webcast series](/handbook/marketing/product-marketing/enablement/)
*  Learn about the [weekly GitLab SDR Enablement webcast series](/handbook/marketing/product-marketing/enablement/#xdr-bdrsdr-coaching)

[Additional Sales Training Resources](./training/additional-resources)
