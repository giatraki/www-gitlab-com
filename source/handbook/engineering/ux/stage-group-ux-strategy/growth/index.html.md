---
layout: handbook-page-toc
title: "Growth UX Team"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview
The Growth area is made up of Fulfillment, Telemetry and four Growth groups which focus on improving specific metrics. We don't have our own product. Instead, we make the experience of paying for GitLab and managing licenses as pleasant as it can be. We also look for strategies to help customers discover the value of the product, thereby increasing the number of customers and users. GitLab believes that **everyone can contribute**, and this is central to our strategy.

See the team that makes up the [Growth Section](/handbook/product/categories/#growth-section).


## Our team
<!--Let's talk about who we are and link to our ReadMe's-->

* [Jacki Bauer](/company/team/#jackib) - UX Manager [Jacki's ReadMe](https://gitlab.com/jackib/jacki-bauer/blob/master/README.md)
* [Tim Noah](/company/team/#timnoah) - Senior Product Designer, Fulfillment
* [Emily Sybrant](/company/team/#esybrant) - Product Designer, Fulfillment
* [Kevin Comoli](/company/team/#kcomoli) - Product Designer, Growth
* [Matej Latin](/company/team/#matejlatin) - Senior Product Designer, Growth [Matej's ReadMe](https://gitlab.com/matejlatin/focus/blob/master/README.md)
* [Evan Read](/company/team/#eread) - Senior Technical Writer
* [Eileen Ruberto](/company/team/#eileenux) - Senior UX Researcher, Fulfillment
* [Jeff Crow](/company/team/#jeffcrow) - Senior UX Researcher, Growth

##### How We Work
We follow the Product Designer and UX Researcher workflows described in the UX section of the handbook. In addition:
* we have issue boards so we can see what everyone is up to. 
    * [by assignee](https://gitlab.com/groups/gitlab-org/-/boards/1254597?label_name[]=UX&label_name[]=devops%3A%3Agrowth)
    * [by group](https://gitlab.com/groups/gitlab-org/-/boards/1334665?&label_name[]=UX&label_name[]=devops%3A%3Agrowth)
    * [by workflow](https://gitlab.com/groups/gitlab-org/-/boards/1346572)
* we **label** our issues with UX, devops::growth and group::.
* we use the [workflow labels](https://gitlab.com/groups/gitlab-org/-/labels?utf8=%E2%9C%93&subscribed=&search=workflow%3A%3A) for regular issues and [experiment workflow labels](/handbook/engineering/development/growth/#experiment-workflow-labels) for experiment issues.
* we use **milestones** to aid in planning and prioritizing the four growth groups of Acquisition, Conversion, Expansion and Retention.
    * PMs provide an [ICE score for experiments](https://docs.google.com/spreadsheets/d/1yvLW0qM0FpvcBzvtnyFrH6O5kAlV1TEFn0TB8KM-Y1s/edit#gid=0) and by using [priority labels](https://docs.gitlab.com/ee/development/contributing/issue_workflow.html#priority-labels) for other issues. 
    * The Product Designer apply the milestone in which they plan to deliver the work (1-2 milestones in advance, or backlog for items that are several months out. For example, if an issue is not doable for a designer in the current milestone, they can add the next milestone to the issue, which will communicate to the PM when the work will be delivered. 
    * If the PM has any concern about the planned milestone, they will discuss trade-offs with the Product Designer and other Growth PMs. 


<!--
To complete this section, we might answer questions like:
- who are we designing for and what is their desired experience?
- how do we align the desired experience to company strategy
- what is our high level approach and philosophy?
These are hard questions! Not meant to be answered in a day, we can do iterative user research to uncover this information over time. -->

### Our customer
Coming soon! We will answer questions like:
- who are the customers we're selling to?
- what are they like at individual or organizational level?
- what value do we provide them?
- what do they think of our product?
- what improvements do they want?
- how do they interact with the product?
- what are the personas we've created?
- what are their purchase concerns? 
- What is their purchase/use journey?


### Our user
Coming soon! We will answer questions like:
- who are the users we're designing for?
- what are they like at individual or organizational level?
- what value do we provide them?
- how can we best communicate that value?
- what do they think of our product?
- what improvements do they want?
- how can we help them engage more with our product?
- how can we help them learn our product?
- how do they interact with the product?
- what are the personas we've created?
- What is their purchase/use journey?

## Customer Journey
As a team we feel that it's really important to understand, document and consider the entire customer journey during design. To that end we will document what we know as we learn it and link that documentation here.

### Customer Journey/Process for Fulfillment/Customer Transactions

When working through transactional issues related to sign-up, trials and upgrades it helps to break down the task into pieces. This way of working through issues enables product designers to document the beginning and end of a user journey in an easily digestible way for everyone. It's based very loosely on a talk from Jared Spool regarding "Content and Design".
* Entry Point(s): The initial touch points of user interactions. (i.e. A Page, CTA or Form etc)
* Decision: Giving users the ability to decide on Products, Options or Packages.
* Confirmation: Summary of a successful or unsuccessful purchase.

These steps won't always be needed and won't always be linear. For instance, an Entry Point may also be a point at which a user selects a Product. 



## Jobs to Be Done
Coming soon! We will list the main JTBD, along with links to helpful material such as an XP baseline, usability score, walkthrough video, journey map, recommendations for improvement, etc. Placeholders are good if we know the JTBD but haven't worked on it.

#### JTBD for Fulfillment
##### Start a GitLab trial
* Job Description: When (situation), I want to (motivation), so I can (expected outcome).
* Baseline Epic (with walkthrough and video): [1332](https://gitlab.com/groups/gitlab-org/-/epics/1332)
* Baseline Issue: [1355](https://gitlab.com/groups/gitlab-org/-/epics/1355)
* Baseline Score: D- (Q2) 
* Recommendations: [1356](https://gitlab.com/groups/gitlab-org/-/epics/1356)

##### Purchase a GitLab Plan
TBD
##### Upgrade a GitLab Plan
TBD
##### Renew a GitLab Plan
TBD
##### Buy an Add-on
TBD
##### Cancel a GitLab Plan
TBD
##### Downgrade a GitLab Plan
TBD


## Our strategy
The Growth UX team will be working closely with Growth PMs to uncover customers core needs and workflows and uncover opportunities to enable customers to get the most out of GitLab. Becoming strategic involves gathering research, looking for patterns, and making plans for the best path forward for our customers and users. It is also about deciding what we value most, and how to best work together to achieve our goals.

<!-- Copied from Secure's strategy list and made a few changes for our team. I think a lot of what they say applies to us, but am open to all suggestions!-->
We are doing activities like this in order to build our strategy:
Our approach to this work includes:
* [Baseline initiative audit and recommendations](/handbook/engineering/ux/experience-baseline-recommendations/) (quarterly)
* Internal understanding: stakeholder interviews (annually)
* Customer understand: user research (ongoing)
* Supporting Growth team experiments
* Rapidly prototyping in order to test ideas
* Performing heuristic evaluations on at least 3 competitors, based competitors the 3 user type is using (annually, ongoing)
* We talk to our customer (ongoing)
* We talk to our users (ongoing)
* We outline current user workflows and improve them (upcoming, ongoing)

Additionally, we value the following:
* A Lean UX approach that entails making a hypothesis, using data and experiments to test the hypothesis, implementing winning ideas, and iterating.
* Testing our features with usefulness and usability studies
* Partnering closely with our internal stakeholders in Legal, Support, Finance and Marketing for feedback and feature adoption
* Partnering with our sales and account team to connect directly with customers and learn why customers did (or didn’t) choose our product
* Billing, settings and other functionality that is secondary to the product should work just as well as the product.
* Prioritizing issues that are likely to increase our number of active users

