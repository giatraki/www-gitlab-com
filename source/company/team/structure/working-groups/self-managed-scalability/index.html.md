---
layout: markdown_page
title: "Self-managed Scalability Working Group"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Attributes

| Property        | Value        |
|-----------------|--------------|
| Date Created    | May 21, 2019 |
| Target End Date | TBD          |
| Slack           | [#wg_sm-scalability](https://gitlab.slack.com/messages/CJBEAQ589) (only accessible from within the company) |
| Google Doc      | [Self-managed Scalability Working Group Agenda](https://docs.google.com/document/d/1H9ENjGO5vNI1e0j3lm2e6zeK8F8o8H-69M3V7m3uYt8/edit) (only accessible from within the company) |
| Issue Board     | [gitlab-org boards 1131633](https://gitlab.com/groups/gitlab-org/-/boards/1131633)

## Business Goal

Ensure all new customers are set up in a standardized environment that will scales with their needs. Migrate existing customers to an appropriate reference environment.

## Exit Criteria (30%)


* [Capture GitLab self-managed performance metrics](https://gitlab.com/groups/gitlab-org/-/epics/806) => `2%`
* [Inventory of self-managed customers with scores of their environment](https://gitlab.com/groups/gitlab-org/-/epics/1338) => `1%` Scoring system being defined.
* [Monitoring enabled by default for all large customers (Prometheus, Grafana and all exporters)](https://gitlab.com/groups/gitlab-org/-/epics/1339) => `20%`
* [Migration strategy and messaging for environmental changes for 10 out-of-spec customers](https://gitlab.com/groups/gitlab-org/-/epics/1340) => `15%` Work track defined to work closely with customers to migrate to our reference architecture. 2/10 customers on work track.
* [List of additional needed reference architectures](https://gitlab.com/gitlab-org/quality/performance/issues/15) => `100%` We have identified 10K, 25K and 50K and the next immediate need.
* [10,000 user reference architecture](https://gitlab.com/groups/gitlab-org/-/epics/1336) => `100%`
* [25,000 user reference architecture](https://gitlab.com/gitlab-org/quality/performance/issues/57) => `20%` Environment created, needs to go through testing.
* [50,000 user reference architecture](https://gitlab.com/gitlab-org/quality/performance/issues/66) => `10%` Environment in-progress.

## Roles and Responsibilities

| Working Group Role    | Person                | Title                          |
|-----------------------|-----------------------|--------------------------------|
| Facilitator           | Mek Stittri           | Director of Quality            |
| Support Lead          | Drew Blessing         | Staff Support Engineer         |
| CS Lead               | Brian Wald            | Solutions Architects Manager   |
| Quality Lead          | Grant Young           | Sr. Software Engineer in Test  |
| Infrastructure Lead   | Ben Kochie            | Senior SRE, Infrastructure     |
| Development Lead      | Matt Nohr             | Engineering Manager, Monitor   |
| PM Lead               | Kenny Johnston        | Director of Product, Ops       |
| Member                | Clement Ho            | Frontend Engineering Manager, Monitor Health |
| Tech Writing Lead     | Achilleas (Axil) Pipinellis | Technical Writer         |
| Member                | Tom Cooney            | Director of Support            |
| Member                | Chun Du               | Director of Engineering        |
| Member                | Francis Potter        | Solutions Architect            |
| Member                | Jesse Lovelace        | Solutions Architects Manager   |
| Member                | John Woods            | TAM Manager                    |
| Member                | Aric Buerer           | Support Engineer               |
| Member                | Tanya Pazitny         | Quality Engineering Manager    |
| Member                | David Sakamoto        | VP of Customer Success         |
| Member                | Andrew Newdigate      | Distinguished SRE              |
| Member                | Nailia Iskhakova      | Software Engineer in Test      |
| Executive Sponsor     | Eric Johnson          | VP of Engineering              |
