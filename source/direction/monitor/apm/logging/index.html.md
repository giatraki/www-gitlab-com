---
layout: markdown_page
title: "Category Vision - Logging"
---

- TOC
{:toc}
# Logging

| | |
| --- | --- |
| Stage | [Monitor](/direction/monitor/) |
| Maturity | [Minimal](/direction/maturity/) |

## Introduction and how you can help
Thanks for visiting this category strategy page on Logging in GitLab. This category belongs to and is maintained by the [APM](/handbook/engineering/development/ops/monitor/APM/) group of the Monitor stage.

This strategy is a work in progress, and everyone can contribute:
 - Please comment and contribute in the linked [issues](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Aapm&label_name[]=Category%3ALogging) and [epics](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Aapm&label_name[]=Category%3ALogging) on this page. Sharing your feedback directly on GitLab.com is the best way to contribute to our strategy and vision.
 - Please share feedback directly via email, Twitter, or on a video call. If you're a GitLab user and have direct knowledge of your Metrics usage, we'd especially love to hear your use case(s)  
 

## Background
A fundamental requirement for running applications is to have a centralized location to manage and review the logs. While manually reviewing logs could work with just a single node app server, once the deployment scales beyond one you need solution which can [aggregate and centralize them](https://gitlab.com/gitlab-org/gitlab-ee/issues/3711) for review.
## Target Audience and Experience
Being able to capture and review logs are an important tool for all users across the DevOps spectrum. From pure developers who may need to troubleshoot their application when it is running in a staging or review environment, as well as pure operators who are responsible for keeping production services online.

The target workflow includes a few important use cases:
1. Aggregating logs from multiple pods and containers from all namespaces
1. Filtering by host, container, service, timespan, regex, and other criteria. These filtering options should align with the filter options and tags/labels of our other monitoring tools, like metrics.
1. Log alerts should also be able to be created, triggering alerts under specific user defined scenarios.

## What's Next & Why
In the distributed nature of cloud-native applications, it is crucial and critical to collect logs across multiple services and infrastructure, present them in an aggregated view, so users could quickly search through a list of logs that originate from multiple pods and containers. 
Therefore, our next step allows our users, with a push of a button to install Elasticsearch on their monitored cluster and collect automatically, all logs from all namespaces in the cluster to facilitate an aggregated logging solution. This enhances our logging capabilities and offers advanced filtering and a full-text search across aggregated logs in a single view.

## Epics
* [Logging - viable](https://gitlab.com/groups/gitlab-org/-/epics/1348)
* [Logging - Complete](https://gitlab.com/groups/gitlab-org/-/epics/1966)

## Competitive Landscape
[Splunk](https://www.splunk.com) and [Elastic](https://www.elastic.co/) are the top two competitors in this space.



