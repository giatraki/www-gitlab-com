---
layout: job_family_page
title: "Frontend Lead"
---

{: .text-center}
<br>

## Responsibilities

* Leads the implementation of significant frontend features
* Works with other teams to identify priorities for releases
* Assigns frontend engineers direction issues
* Identifies hiring needs for frontend group
* Interviews candidates for Frontend engineers positions
* Continues to spend part of their time coding
* Ensures that the technical decisions and process set by the CTO are followed
* Does 1:1's with all reports every 2-5 weeks (depending on the experience of the report)
* Is available for 1:1's on demand of the report
* Uses the contributor analytics to ensure that Frontend engineers that are stuck are helped
* Ensures quality implementation of design materials
* Prioritize frontend issues which lead to bad user experience
* Review of the merge requests made by Frontend engineers
* Delivers input on promotions, function changes, demotions and firings in consultation with the CEO, CTO, and VP of Engineering
* Defines best practices and coding standards for frontend group

## Requirements for Candidates

* At least 1 year of experience leading, mentoring, and training teams of frontend engineers
* Experience working on a production-level JavaScript applications
* Self-motivated and strong organizational skills
* Strong understanding of CSS
* Expert knowledge of JavaScript
* Passionate about testing and design principles
* Basic knowledge of Vue.js is a plus but not a requirement
* Collaborate effectively with Product Designers, Developers, and Designers
* Be able to work with the rest of the community
* Knowledge of Ruby on Rails is a plus
* You share our [values](/handbook/values), and work in accordance with those values.
* [Leadership at GitLab](https://about.gitlab.com/handbook/leadership/#management-group)
* Ability to use GitLab

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/company/team).

* Qualified candidates receive a short questionnaire from our Global Recruiters
* Selected candidates will be invited to schedule a 45 minute [screening call](/handbook/hiring/#screening-call) with our Global Recruiters
* Next, candidates will be invited to schedule a 45 minute first interview with a Frontend Engineer
* Candidates will then be invited to schedule a 1 hour technical interview with the Frontend Lead
* Candidates will be invited to schedule a third 45 minute interview with our VP of Engineering
* Finally, candidates will schedule a 50 minute interview with our CEO
* Successful candidates will subsequently be made an offer via email

Additional details about our process can be found on our [hiring page](/handbook/hiring).

## Relevant links

- [Engineering Handbook](/handbook/engineering)
- [Engineering Workflow](/handbook/engineering/workflow)
