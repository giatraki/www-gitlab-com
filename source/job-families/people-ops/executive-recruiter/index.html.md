---
layout: job_family_page
title: Executive Recruiter
---

## Executive Recruiter

The Executive Recruiter is fully responsible for owning and executing executive searches across all business functions/geographies. They are a highly motivated individual who works to create a positive experience for candidates and hiring teams.  They are a dynamic team member who owns and drives executive searches across the engineering, sales and business operations functions, identify amazing candidates, improve existing hiring practices, and deliver exceptional customer service. GitLab strives to be a preferred employer and relies on all members of the Recruiting Team to act as brand ambassadors by embodying the company values and identifying those values in potential team members. The Executive Recruiter is dedicated to helping build a qualified, diverse, and motivated team as we scale, they are well-versed in all aspects of an executive search including creative candidate sourcing, candidate generation and development, client management and offer extension/negotiation.

### Responsibilities

* Conducting research to map target companies and the right candidates for GitLab
* Creating robust search strategies that will identify and attract the best and brightest
* Serving as GitLab’s expert for leadership recruiting, including working on continuous company and talent mapping in key areas for the company.
* Developing innovative ways to raise the bar on how GitLab recruits senior talent.
* Developing those prospects into candidates, engaging them and performing assessment/screening
* Continually contributing to the knowledge base of the Recruiting Team and to GitLab as a whole, by providing education on relevant industries and talent pools/profiles
* Building out talent networks and tapping knowledgeable industry sources to develop an on-going pool of candidates
* Leading client update meetings on active searches
* Conducting interviews/reference checks and presenting/negotiating employment offers on behalf of GitLab.
* Partnering with hiring executives to calibrate on talent and providing strategic guidance/advice.
* Develop search strategies, conduct research, perform thorough assessments and present slates of qualified candidates.

#### Requirements

* 10+ years experience recruiting at all levels, preferably in a global capacity within the software industry.  A blend of in-house and search firm/agency experience is preferred.
* Proven success in owning and successfully executing executive roles at scale for top-tier tech companies.
* Focused on delivering an excellent candidate experience
* Ambitious, efficient and stable under tight deadlines and competing priorities
* Remote working experience in a technology startup will be an added advantage
* Ability to build global relationships with managers and colleagues across multiple disciplines and timezones
* Outstanding written and verbal communication skills across all levels
* Willingness to learn and use software tools including Git and GitLab
* College / University degree in Marketing, Human Resources or related field from an accredited institution preferred
* Successful completion of a [background check](/handbook/people-group/code-of-conduct/#background-checks).
* Share our [values](/handbook/values), and work in accordance with those values.
* Ability to use GitLab

## Performance Indicators

* [Hire vs Plan](https://about.gitlab.com/handbook/hiring/metrics/#hires-vs-plan)
* [Time to Offer Accept](https://about.gitlab.com/handbook/hiring/metrics/#time-to-offer-accept-days)
* [Offer Accepteance Rate](https://about.gitlab.com/handbook/hiring/metrics/#offer-acceptance-rate)
* [New Hire Location Factor](https://about.gitlab.com/handbook/hiring/metrics/#new-hire-location-factor)
* [Average Candidate ISAT](https://about.gitlab.com/handbook/hiring/metrics/#interviewee-satisfaction-isat)

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](https://about.gitlab.com/company/team/).

* Qualified candidates will be invited to schedule a 25-minute screening call with the VP of Recruiting,
* Next, candidates will be invited to schedule a 25-minute call with a Recruiting Manager,
* Then, candidates will be invited to schedule 50-minute interviews with the Cheif Financial Officer (CFO), Cheif People Officer (CPO), Cheif Revenue Officer (CRO) and the VP or Engineering,
* Finally, candidates will be invited to a 50-minute interview with the CEO

As always, the interviews and screening call will be conducted via a [video call](https://about.gitlab.com/handbook/communication/#video-calls). See more details about our interview process [here](https://about.gitlab.com/handbook/hiring/interviewing/).
